<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$sex="";
$facultate="";
$error=0;

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

//creez sex
if(isset($_POST['sex'])){
	$sex = $_POST['sex'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
//Aici creez campul facultate
if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}
//Validez pentru a nu putea ramane campuri necompletate
if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($check) || empty($captcha_inserted ) || empty($facultate)){
	$error = 1;
	$error_text = "One or more fields are empty!";
	//Validez numarul de telefon
if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid!";
}
}
//Validez pentru a ma asigura ca numele si prenumele sunt corecte
if(strlen($firstname) < 3 || strlen($firstname)>20 || strlen($lastname) < 3 || strlen($lastname)>20){
	$error = 1;
	$error_text = "First or Last name is shorter than expected!";
}
//Validez astfel incat campul question sa fie corect
if(strlen($question)<15){
	$error=1;
	$error_text="Question is not valid!";
}


//Validez CNP-ul
if(strlen($cnp)<13 || !is_numeric($cnp) || $cnp[0]==7 || $cnp[0]==8 || $cnp[0]==9 || $cnp[0]==0){
	$error=1;
	$error_text="CNP is not valid!";
}
//Validez varsta(am pus data actuala 01.01.2019 si cea mai mica putand fii din 2001) 
if($birth[0]*1000+$birth[1]*100+$birth[2]*10+$birth[3]>2001 || $birth[0]*1000+$birth[1]*100+$birth[2]*10+$birth[3]<=1919){
	$error=1;
	$error_text="Varsta is not valid!";
}
//Validez captcha
if(strlen($captcha_generated)==strlen($captcha_inserted)){
	for($i=0;$i<=strlen($captcha_inserted);$i++){
		if($captcha_generated[$i]!=$captcha_inserted[$i])
			$error=1;
			$error_text="Wrong captcha";
			break;
	}
}
//Validez adresa de e-mail 
for($i=0;$i<strlen($email);$i++){
	$arond=0;
	$pct=0;
	if($email[$i]=='@') $arond++;
	if($email[$i]=='.') $pct++;
	if($i==strlen($email) && ($arond!=1 || $pct!=1) ){
		$error=1;
		$error_text="Email is not valid!";
	}
}
//Validez campul facultate
if(strlen($facultate)<3 || strlen($facultate)>30){
		$error=1;
		$error_text="Facultate is not vaid!";
}


//Validez adresa de Facebook
$link="https://www.facebook.com/";
for($i=1;$i<strlen($link);$i++){
	if($link[$i]!=$facebook[$i]){
		$error=1;
		$error_text="Facebook incorect";
	}

}
try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
//Verific dublarea 
$query = $con -> prepare("SELECT 'email' FROM 'register2' WHERE 'email' = ?");
$query -> bindValue(1,$email);
$query -> execute();
if($query->rowCount() !=0 ){
	$error=1;
	$error_text="Email-ul exista deja";
}
//Includ optiunea de a nu se putea inscrie mai mult de 50 de persoane
$query2 = $con -> prepare("SELECT * FROM 'register2'");
$query2 -> execute();
if($query2 -> rowCount() >= 50 ){
	$error=1;
	$error_text="Prea multi participanti";
}
if($error==0){

$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,facultate,sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question,:facultate,:sex)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
//Adaug in baza de date facultatea
$stmt2 -> bindParam(':facultate',$facultate);
//Verific daca data de nastere si cea din CNP coincid
if($cnp[1]!=$birth[2] || $cnp[2]!=$birth[3] || $cnp[3]!=$birth[5] || $cnp[4]!=$birth[6] || $cnp[5]!=$birth[8] || $cnp[6]!=$birth[9] ){
	$error=1;
	$error_text="Data de nastere si CNP incompatibile";
}
//Validez sexul
if($cnp[0]==1 || $cnp[0]==3 || $cnp[0]==5){
	$sex='M';
	$stmt2 -> bindParam(':sex',$sex);
}
else {
	$sex='F';
	$stmt2 -> bindParam(':sex',$sex);
}

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";


}else{
    echo "Succes";
}
}
else {
	echo $error_text;
}
?>